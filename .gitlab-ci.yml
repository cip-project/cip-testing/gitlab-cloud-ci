# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: MIT
#
---
variables:
  GIT_STRATEGY: clone
  http_proxy: "$HTTP_PROXY"
  https_proxy: "$HTTPS_PROXY"
  ftp_proxy: "$FTP_PROXY"
  no_proxy: "$NO_PROXY"

stages:
  - test
  - build

test:
  stage: test
  image: nixos/nix:latest
  before_script:
    - nix-env -iA cachix -f https://cachix.org/api/v1/install
    - cachix use gitlab-cloud-ci
    - nix --extra-experimental-features "nix-command flakes" profile install 'nixpkgs#jq'
  script:
    # build default package so that cachix can cache it
    - nix build --json --extra-experimental-features "nix-command flakes" | jq -r '.[].outputs | to_entries[].value' | cachix push gitlab-cloud-ci
    - nix --extra-experimental-features "nix-command flakes" develop '.#ci' --command just format lint generate
    - nix --extra-experimental-features "nix-command flakes" develop '.#ci' --command git diff --exit-code

reuse:
  stage: test
  needs: []
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

build-container:
  stage: build
  image: nixos/nix:latest
  before_script:
    - nix-env -iA cachix -f https://cachix.org/api/v1/install
    - cachix use gitlab-cloud-ci
    - nix --extra-experimental-features "nix-command flakes" profile install 'nixpkgs#skopeo' 'nixpkgs#jq'
    - skopeo login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - |
      mkdir -p /etc/containers
      echo '{"default":[{"type":"insecureAcceptAnything"}],"transports":{"docker-daemon":{"":[{"type":"insecureAcceptAnything"}]}}}' \
          > /etc/containers/policy.json
  script:
    - nix build --json --extra-experimental-features "nix-command flakes" '.#docker' | jq -r '.[].outputs | to_entries[].value' | cachix push gitlab-cloud-ci
    - skopeo copy docker-archive:result docker://$CI_REGISTRY_IMAGE:${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
  only:
    - tags
    - next
