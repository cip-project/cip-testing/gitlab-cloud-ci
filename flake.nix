# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

{
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";
  inputs.utils.url = "github:numtide/flake-utils";

  outputs = { nixpkgs, utils, self }: utils.lib.eachDefaultSystem (system:
    let

      pkgs = import nixpkgs { inherit system; };

      runtimeDeps = with pkgs; [
        kops_1_30 # XXX: consider updating kops
        kubernetes-helm
        ansible
        k3sup
      ];

      pythonDeps = with pkgs.python3Packages;  [
        boto3
        coloredlogs
        deepmerge
        humanize
        kubernetes
        paramiko
        pyyaml
        sh
        shtab
        tzlocal
        # if you change something here, don't forget to update docker.nix
      ];

      ciDeps = with pkgs; [ python3Packages.pytest python3Packages.flake8 black just ];

      gitlabci = with pkgs; python3Packages.buildPythonApplication {
        pname = "gitlabci";
        version = lib.removeSuffix "\n" (builtins.readFile ./VERSION.txt);

        src = ./.;

        nativeBuildInputs = [ makeWrapper installShellFiles ];
        nativeCheckInputs = with python3Packages; [ pytestCheckHook ];
        propagatedBuildInputs = pythonDeps;

        makeWrapperArgs = [
          "--prefix PATH : ${lib.makeBinPath runtimeDeps}"
        ];

        postInstall = ''
          installShellCompletion --cmd gitlabci \
            --bash <($out/bin/gitlabci completion bash) \
            --zsh <($out/bin/gitlabci completion zsh) \
        '';
      };

    in
    {

      packages = {
        default = gitlabci;
        docker = import ./docker.nix { inherit pkgs gitlabci runtimeDeps; };
      };

      devShells = with pkgs; {
        default = mkShell {
          buildInputs = runtimeDeps ++ pythonDeps ++ ciDeps ++ [ awscli2 kubectl ];
          shellHook = ''
            export HELM_TLS_ENABLE=true
            mkdir -p secrets/kube && chmod 700 secrets
            CLUSTER_NAME=gitlabci
            if [[ -e config.yaml ]]; then
              CLUSTER_NAME=$(${pkgs.yq}/bin/yq -r .aws.cluster_name < config.yaml)
            fi
            export CLUSTER_NAME
            export KUBECONFIG=$PWD/secrets/kube/$CLUSTER_NAME
            export KOPS_STATE_STORE="s3://$CLUSTER_NAME-kops-state-store"
          '';
        };
        ci = mkShell {
          buildInputs = runtimeDeps ++ pythonDeps ++ ciDeps;
        };
      };

    });

}
