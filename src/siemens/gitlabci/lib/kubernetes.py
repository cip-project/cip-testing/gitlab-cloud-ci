# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging

from kubernetes import client, config

from kubernetes.client.rest import ApiException

log = logging.getLogger(__name__)


def configure_kubernetes_client():
    try:
        log.debug("Loading local kube config")
        config.load_kube_config()
        log.debug("Successfully loaded local kube config")
    except FileNotFoundError:
        log.info("No kube config file found (yet)")
    except config.config_exception.ConfigException:
        log.warning("kubeconfig is invalid or incomplete")


def k8s_is_deployed(name, ns):
    try:
        client.AppsV1Api().read_namespaced_deployment_status(name, ns)
        return True
    except ApiException as e:
        if e.status == 404:
            return False
        else:
            raise e


def k8s_is_deployment_ready(name, ns):
    try:
        response = client.AppsV1Api().read_namespaced_deployment_status(name, ns)
        log.debug("deployment status: %s", response.status)
        if response.status.conditions:
            for condition in response.status.conditions:
                if condition.type == "Available":
                    status = condition.status
                    log.debug("deployment %s available status: %s", name, status)
                    return status == "True"
        else:
            return False
    except ApiException as e:
        if e.status == 404:
            return False
        else:
            raise e
    return False


def tolerate_deployment_on_master(name, ns):
    log.info(
        "Allowing deployment %s in namespace %s to be scheduled on the master node",
        name,
        ns,
    )
    patch = {
        "spec": {
            "template": {
                "spec": {
                    "tolerations": [
                        {
                            "key": "node-role.kubernetes.io/control-plane",
                            "effect": "NoSchedule",
                        }
                    ]
                }
            }
        }
    }
    client.AppsV1Api().patch_namespaced_deployment(name, ns, patch)


def pin_deployment_on_master(name, ns):
    log.info("Pinning deployment %s in namespace %s on master node", name, ns)
    patch = {
        "spec": {
            "template": {
                "spec": {
                    "selector": {
                        "matchExpressions": [
                            {
                                "key": "node-role.kubernetes.io/control-plane",
                                "operator": "Exists",
                            },
                        ],
                    },
                    "tolerations": [
                        {
                            "key": "node-role.kubernetes.io/control-plane",
                            "effect": "NoSchedule",
                        }
                    ],
                }
            }
        }
    }
    k8s_client_apps = client.AppsV1Api()
    k8s_client_apps.patch_namespaced_deployment(name, ns, patch)


def create_namespace(name):
    core = client.CoreV1Api()
    ns_found = False
    for ns in core.list_namespace().items:
        if ns.metadata.name == name:
            ns_found = True
            break
    if not ns_found:
        log.debug("Creating namespace %s", name)
        namespace_manifest = {
            "apiVersion": "v1",
            "kind": "Namespace",
            "metadata": {"name": name, "resourceversion": "v1"},
        }
        core.create_namespace(body=namespace_manifest)
        log.info("Created namespace %s", name)
