# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import json
import sh

log = logging.getLogger(__name__)


def install_helm_repo(name: str, url: str):
    """Install the given repo if it doesn't exist yet"""
    try:
        output = sh.helm("repo", "list", "--output=json")
        repos = json.loads(output)
        for item in repos:
            if item["name"] == name:
                return
    except Exception:
        pass
    sh.helm("repo", "add", name, url)
    sh.helm("repo", "update")
