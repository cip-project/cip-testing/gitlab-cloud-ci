# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os
import os.path
import typing

import paramiko

from paramiko import (
    SSHClient,
    ProxyCommand,
    AutoAddPolicy,
    RSAKey,
    DSSKey,
    ECDSAKey,
    Ed25519Key,
    SSHConfig,
    SSHException,
)
from paramiko.config import SSHConfigDict

log = logging.getLogger(__name__)


class SSHSession:
    def __init__(
        self,
        host: str,
        config_path: typing.Union[str, None],
    ):
        """create a ssh session to host name"""
        ssh_config = read_sshconfig(host, config_path)

        sock = None
        if "proxycommand" in ssh_config:
            sock = ProxyCommand(ssh_config["proxycommand"])

        ssh_client = SSHClient()
        ssh_client.load_system_host_keys()
        ssh_client.set_missing_host_key_policy(AutoAddPolicy())

        identify_file = find_identify_file(ssh_config)
        if not identify_file:
            raise RuntimeError("IdentityFile not found")

        ssh_client.connect(
            hostname=ssh_config["hostname"],
            username=ssh_config["user"],
            port=ssh_config.as_int("port"),
            pkey=read_sshkey(identify_file),
            sock=sock,
        )
        self.client = ssh_client

    def execute_command(self, command, input_str=None):
        """execute the given command and print output/errors"""
        transport = self.client.get_transport()
        if not transport:
            raise RuntimeError("Failed to get transport")
        channel = transport.open_session()
        try:
            channel.get_pty()
            file_desc = channel.makefile()
            channel.exec_command(command)
            if input_str:
                stdin = channel.makefile("wb")
                stdin.write(input_str)
                stdin.flush()

            for line in iter(file_desc.readline, ""):
                line = line.strip()
                if "error" in line.casefold():
                    log.error(line)
                elif "warning" in line.casefold():
                    log.warning(line)
                else:
                    log.info(line)
        finally:
            channel.close()

    def close(self):
        """close the ssh session"""
        self.client.close()


def read_sshkey(filename: str) -> paramiko.PKey:
    """read the ssh key file and generate the key for the ssh session"""
    ex = RuntimeError("Error reading ssh key")
    for from_private_key_file in [
        RSAKey.from_private_key_file,
        ECDSAKey.from_private_key_file,
        Ed25519Key.from_private_key_file,
        DSSKey.from_private_key_file,
    ]:
        try:
            return from_private_key_file(filename)
        except SSHException as e:
            ex = e
    raise ex


def pkey_to_pub_key(pkey: paramiko.PKey) -> typing.Union[str, None]:
    """extract public key from private key"""
    return "{} {}".format(pkey.get_name(), pkey.get_base64())


def read_sshconfig(host: str, config_path: typing.Union[str, None]) -> SSHConfigDict:
    if not config_path:
        config_path = os.path.expanduser(os.path.join("~", ".ssh", "config"))
    config = SSHConfig.from_path(os.path.expanduser(config_path))
    return config.lookup(host)


def find_identify_file(ssh_config: SSHConfigDict) -> typing.Union[str, None]:
    for fname in ssh_config["identityfile"]:
        try:
            os.stat(fname)
            return fname
        except FileNotFoundError:
            pass
