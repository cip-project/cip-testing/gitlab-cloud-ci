# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import sys
import logging
import sh
import json

log = logging.getLogger(__name__)


def install_collection(name: str):
    """Install the given role with ansible-galaxy"""
    output = sh.ansible_galaxy("collection", "list", "--format=json")
    data = json.loads(output)
    for values in data.values():
        for k in values.keys():
            if k == name:
                return
    sh.ansible_galaxy("collection", "install", name)


def run_playbook(playbook_file, inventory_file, interactive):
    """run the given ansible playbook with the given file"""
    log.info(
        "run ansible playbook.\
    If a passphrase protects the ssh key enter the passphrase if requested."
    )
    # put ansible into the foreground for passphrase protected ssh keys
    if interactive:
        sh.ansible_playbook(
            "--inventory",
            inventory_file,
            "--become",
            "--ask-pass",
            playbook_file,
            _fg=True,
        )
    else:
        sh.ansible_playbook(
            "--inventory",
            inventory_file,
            "--become",
            "--ssh-extra-args",
            "-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no",
            playbook_file,
            _out=sys.stdout,
        )
