# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Silvano Cirujano Cuesta <silvano.cirujano-cuesta@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import sys
import logging
import os

import sh
import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


log = logging.getLogger(__name__)


def kops(*kops_args, **sh_kwargs):
    try:
        return sh.kops(*kops_args, **sh_kwargs, _err=sys.stderr)
    except sh.ErrorReturnCode:
        log.error("kOps has failed calling: " + " ".join(kops_args))
        raise


def export_kubecfg(cluster_name, force=False):
    """Export kubeconfig and set proxy variable"""
    cfg_file = os.getenv("KUBECONFIG", "kubeconfig")
    if force:
        try:
            os.remove(cfg_file)
        except FileNotFoundError:
            pass

    try:
        os.stat(cfg_file)
        return
    except FileNotFoundError:
        pass

    full_cluster_name = "{}.k8s.local".format(cluster_name)
    try:
        sh.kops(
            "export",
            "kubeconfig",
            "--admin",
            "--name",
            full_cluster_name,
        )
        proxy = os.getenv("https_proxy")
        if proxy:
            # see https://github.com/kubernetes/website/pull/32245
            doc = None
            with open(cfg_file, "r") as f:
                doc = yaml.load(f, Loader=Loader)
                doc["proxy-url"] = proxy
            if doc:
                with open(cfg_file, "w") as f:
                    yaml.dump(doc, f, Dumper=Dumper)
    except sh.ErrorReturnCode as e:
        log.warn("Failed to export kubeconfig: %s", e)
        try:
            os.remove(cfg_file)
        except FileNotFoundError:
            pass
