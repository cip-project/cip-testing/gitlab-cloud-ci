# Copyright (c) Siemens AG, 2019 - 2020
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os
import logging
import coloredlogs
import shtab
import argparse

import siemens.gitlabci.cmd.autoscaler as autoscaler
import siemens.gitlabci.cmd.create as create
import siemens.gitlabci.cmd.dashboard as dashboard
import siemens.gitlabci.cmd.destroy as destroy
import siemens.gitlabci.cmd.jobs as jobs
import siemens.gitlabci.cmd.monitoring as monitoring
import siemens.gitlabci.cmd.runner as runner
import siemens.gitlabci.cmd.hardening as hardening
import siemens.gitlabci.cmd.status as status
import siemens.gitlabci.cmd.ssh as ssh

from siemens.gitlabci.config import Config


def main():
    log = logging.getLogger()
    coloredlogs.install(
        logger=log,
        level="INFO",
        fmt="%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] "
        "%(message)s",
        datefmt="[%Y-%m-%d] %H:%M:%S",
    )

    try:
        cfg = Config("config.yaml")
        os.environ["KOPS_STATE_STORE"] = "s3://{}-kops-state-store".format(
            cfg.aws["cluster_name"]
        )
    except FileNotFoundError:
        log.warn("config.yaml not found")

    parser = get_main_parser()
    args = parser.parse_args()
    args.func(args)


def get_main_parser():
    main_parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    subparsers = main_parser.add_subparsers(required=True, dest="subcommand")

    parser = subparsers.add_parser("completion", help="generate shell completion")
    shtab.add_argument_to(parser, "shell", parent=main_parser)

    parser = subparsers.add_parser("autoscaler", help="manage autoscaler")
    autoscaler.add_parsers(parser)

    parser = subparsers.add_parser("create", help="create new cluster")
    create.add_parsers(parser)

    parser = subparsers.add_parser("dashboard", help="kubernetes dashboard")
    dashboard.add_parsers(parser)

    parser = subparsers.add_parser("destroy", help="destroy existing cluster")
    destroy.add_parsers(parser)

    parser = subparsers.add_parser("jobs", help="job management")
    jobs.add_parsers(parser)

    parser = subparsers.add_parser("monitoring", help="cluster monitoring")
    monitoring.add_parsers(parser)

    parser = subparsers.add_parser("runner", help="runner management")
    runner.add_parsers(parser)

    parser = subparsers.add_parser("hardening", help="hardening measures")
    hardening.add_parsers(parser)

    parser = subparsers.add_parser("status", help="show cluster status")
    status.add_parsers(parser)

    parser = subparsers.add_parser("ssh", help="ssh into control plane")
    ssh.add_parsers(parser)

    return main_parser


if __name__ == "__main__":
    main()
