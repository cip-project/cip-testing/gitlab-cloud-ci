# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import coloredlogs
import logging
import humanize

from kubernetes import client
from datetime import datetime
from tzlocal import get_localzone
from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client
from siemens.gitlabci.cmd.runner.list import _list as runner_list
from siemens.gitlabci.cmd.jobs.list import _list as jobs_list
from siemens.gitlabci.lib.kops import export_kubecfg


log = logging.getLogger(__name__)


def add_parsers(parent_parser):
    parent_parser.add_argument(
        "--namespace",
        default="gitlab",
        help="use the given namespace",
        dest="namespace",
    )
    parent_parser.set_defaults(func=status)


def status(args):
    """Show cluster status"""

    cfg = load_config()
    namespace = args.namespace
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    configure_kubernetes_client()

    coloredlogs.set_level("ERROR")

    print_header("Nodes")
    node_fmt_str = "{0: <22}{1:24}{2: <8}{3: <16}{4: <30}{5}"
    print(node_fmt_str.format("NAME", "IP", "STATUS", "ROLES", "AGE", "VERSION"))

    nodes = None
    try:
        nodes = client.CoreV1Api().list_node()
    except Exception:
        log.info("Something went wrong, trying again")
        export_kubecfg(cluster_name, force=True)
        configure_kubernetes_client()
        nodes = client.CoreV1Api().list_node()

    control_plane_name = ""
    if nodes:
        for item in nodes.items:
            external_ip = ""
            for x in item.status.addresses:
                if x.type == "ExternalIP":
                    external_ip = x.address
            # name, status, roles, age, version
            status = "up" if item.status.conditions[-1].status == "True" else "down"
            age = datetime.now(get_localzone()) - item.metadata.creation_timestamp
            role = (
                "control-plane"
                if "node-role.kubernetes.io/control-plane" in item.metadata.labels
                else "node"
            )
            name = item.metadata.name
            if role == "control-plane":
                control_plane_name = name
            version = item.status.node_info.kubelet_version
            print(
                node_fmt_str.format(
                    name,
                    external_ip,
                    status,
                    role,
                    humanize.naturaldelta(age),
                    version,
                )
            )

    print_header("Control Plane Pods")
    v1 = client.CoreV1Api()
    fmt_str = "{0: <80}{1: <10}{2: <10}{3: <10}"
    print(fmt_str.format("NAME", "READY", "RESTARTS", "AGE"))
    for item in v1.list_pod_for_all_namespaces().items:
        if item.spec.node_name == control_plane_name:
            created = item.metadata.creation_timestamp
            age = datetime.now(get_localzone()) - created
            ready = [x.ready for x in item.status.container_statuses]
            n = len(ready)
            ok = len([x for x in ready if x])
            restart_count = sum(
                [x.restart_count for x in item.status.container_statuses]
            )
            name = item.metadata.name
            print(
                fmt_str.format(
                    name, "%d/%d" % (ok, n), restart_count, humanize.naturaldelta(age)
                )
            )

    print_header("Gitlab Runners")
    runner_list(namespace)

    print_header("CI Jobs")
    jobs_list(namespace)


def print_header(title):
    print(
        """
###############################################################################
## %s
###############################################################################
"""
        % title
    )
