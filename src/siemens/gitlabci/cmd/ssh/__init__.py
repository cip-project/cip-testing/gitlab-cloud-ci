# Copyright (c) Siemens AG, 2024
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import coloredlogs
import logging
import subprocess

from kubernetes import client
from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client
from siemens.gitlabci.lib.kops import export_kubecfg


log = logging.getLogger(__name__)


def add_parsers(parent_parser):
    parent_parser.set_defaults(func=ssh)


def ssh(args):
    """ssh into control plane"""

    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    configure_kubernetes_client()

    coloredlogs.set_level("ERROR")

    nodes = []
    try:
        nodes = client.CoreV1Api().list_node().items
    except Exception:
        log.info("Something went wrong, trying again")
        export_kubecfg(cluster_name, force=True)
        configure_kubernetes_client()
        nodes = client.CoreV1Api().list_node().items

    ccp = next(
        filter(
            lambda x: "node-role.kubernetes.io/control-plane" in x.metadata.labels,
            nodes,
        )
    )
    external_ip = next(
        filter(lambda x: x.type == "ExternalIP", ccp.status.addresses)
    ).address
    subprocess.call(
        [
            "ssh",
            "-i",
            cfg.aws["ssh_private_key"],
            "-o",
            "StrictHostKeyChecking=no",
            "-o",
            "UserKnownHostsFile=/dev/null",
            f"ubuntu@{external_ip}",
        ]
    )
