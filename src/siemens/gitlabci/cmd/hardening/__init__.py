# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
from . import deploy


def add_parsers(parent_parser):
    subparsers = parent_parser.add_subparsers(required=True, dest="subcommand")
    parser = subparsers.add_parser("deploy", help="apply hardening measures")

    parser.add_argument(
        "--inventory",
        default="share/k8s/hardening/inventory",
        help="specify the ansible inventory",
        dest="inventory",
    )
    parser.add_argument(
        "--playbook",
        default="share/k8s/hardening/hardening.yaml",
        help="specify the playbook to harden the Kubernetes cluster",
        dest="playbook",
    )
    parser.add_argument(
        "--interactive",
        default=False,
        action="store_true",
        help="run in interactive mode, e.g. ask for ssh key password",
        dest="interactive",
    )
    parser.set_defaults(func=deploy.run)
