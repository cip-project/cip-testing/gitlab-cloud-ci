# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import tempfile
import logging

from string import Template
from kubernetes import client
from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.aws import on_aws
from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client
from siemens.gitlabci.lib.ansible import install_collection, run_playbook

log = logging.getLogger(__name__)


def run(args):
    """deploy hardening"""

    inventory = args.inventory
    playbook = args.playbook
    interactive = args.interactive

    cfg = load_config()
    hosts = []

    key_file = None
    configure_kubernetes_client()
    k8s_client_core = client.CoreV1Api()
    port = 22
    if on_aws(k8s_client_core):
        user = "ubuntu"
        key_file = cfg.aws["ssh_private_key"]
        cluster_name = cfg.aws["cluster_name"]
        export_kubecfg(cluster_name)
        configure_kubernetes_client()
        k8s_client_core = client.CoreV1Api()
        # get all hosts with external dns
        for row in k8s_client_core.list_node().items:
            addresses = row.status.addresses
            for address in addresses:
                if address.type == "ExternalDNS":
                    hosts.append(address.address)
    else:
        key_file = cfg.ssh["ssh_private_key"]
        port = cfg.ssh["port"]
        user = cfg.ssh["user"]
        hosts.append(cfg.ssh["host"])

    install_collection("devsec.hardening")

    if len(hosts) == 0:
        raise RuntimeError("No hosts found")

    with open(playbook, mode="r") as playbook_file:
        ansible_playbook = Template(playbook_file.read())

    with open(inventory, mode="r") as inventory_file:
        ansible_inventory = Template(inventory_file.read())

    for host in hosts:
        log.info("Applying hardening to host %s", host)
        overwrites = {"host": host, "port": port, "user": user, "key_file": key_file}

        with tempfile.NamedTemporaryFile(
            mode="w"
        ) as temp_playbook, tempfile.NamedTemporaryFile(mode="w") as temp_inventory:
            merged_playbook = ansible_playbook.substitute(overwrites)
            merged_inventory = ansible_inventory.substitute(overwrites)
            temp_playbook.write(merged_playbook)
            temp_inventory.write(merged_inventory)
            temp_playbook.flush()
            temp_inventory.flush()

            run_playbook(temp_playbook.name, temp_inventory.name, interactive)
