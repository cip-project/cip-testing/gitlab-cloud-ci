# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import sh

from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kops import export_kubecfg

log = logging.getLogger(__name__)


def run(args):
    """Purge Kubernetes dashboard"""
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)

    log.info("Uninstalling Kubernetes dashboard")
    sh.helm("uninstall", "kubernetes-dashboard", "--namespace=kubernetes-dashboard")
