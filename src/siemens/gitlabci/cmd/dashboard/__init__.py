# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

from . import deploy, get_token, purge


def add_parsers(parent_parser):
    subparsers = parent_parser.add_subparsers(required=True, dest="subcommand")
    parser = subparsers.add_parser("deploy", help="deploy Kubernetes dashboard")
    parser.set_defaults(func=deploy.run)

    parser = subparsers.add_parser(
        "get-token", help="get token to login to K8s dashboard"
    )
    parser.set_defaults(func=get_token.run)

    parser = subparsers.add_parser("purge", help="purge Kubernetes dashboard")
    parser.set_defaults(func=purge.run)
