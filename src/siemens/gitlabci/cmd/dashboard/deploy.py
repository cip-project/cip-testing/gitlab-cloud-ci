# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import sh

from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.kubernetes import (
    configure_kubernetes_client,
    create_namespace,
)
from kubernetes import client, utils

log = logging.getLogger(__name__)


def run(_args):
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    configure_kubernetes_client()
    create_namespace("kubernetes-dashboard")
    k8s_client = client.ApiClient()
    try:
        for fname in [
            "share/k8s/dashboard/01-create-service-account.yml",
            "share/k8s/dashboard/02-create-role-binding.yml",
        ]:
            utils.create_from_yaml(k8s_client, fname)
    except Exception as e:
        log.warning("Error creating dashboard user: %s", e)

    sh.helm("repo", "add", "dashboard", "https://kubernetes.github.io/dashboard/")
    sh.helm("repo", "update")
    sh.helm(
        "install",
        "dashboard/kubernetes-dashboard",
        "--name-template",
        "kubernetes-dashboard",
        "--namespace",
        "kubernetes-dashboard",
        "-f",
        "./share/k8s/dashboard/values.yaml",
    )
    log.info("Successfully deployed K8s dashboard")
