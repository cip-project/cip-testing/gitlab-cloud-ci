# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging

from kubernetes import client
from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client

log = logging.getLogger(__name__)


def run(_args):
    """Get token to login to K8s dashboard"""
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    configure_kubernetes_client()
    core = client.CoreV1Api()
    spec = client.V1TokenRequestSpec(expiration_seconds=3600, audiences=[])
    req = client.AuthenticationV1TokenRequest(spec=spec)
    result = core.create_namespaced_service_account_token(
        "admin-user", "kubernetes-dashboard", req
    )
    print(result.status.token)
