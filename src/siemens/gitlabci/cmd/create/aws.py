# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os
import sys
import tempfile
import time

import yaml
import boto3

from kubernetes import client

from siemens.gitlabci.config import load_config

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from siemens.gitlabci.cmd.autoscaler.deploy import _deploy as deploy_autoscaler
from siemens.gitlabci.lib.ssh import pkey_to_pub_key, read_sshkey

if not os.getenv("_GITLABCI_COMPLETE", None):
    from siemens.gitlabci.lib.kops import kops, export_kubecfg
    from siemens.gitlabci.lib.aws import s3_exists_bucket
    from siemens.gitlabci.lib.kubernetes import (
        configure_kubernetes_client,
        tolerate_deployment_on_master,
    )

log = logging.getLogger(__name__)


def run(args):
    """Create ISAR CI setup running on AWS"""
    kubernetes_version = args.kubernetes_version
    enable_metrics = args.enable_metrics
    enable_autoscaler = args.enable_autoscaler

    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    region = cfg.aws["region"]

    s3 = boto3.resource("s3", region_name=region)
    bucket_name = "{}-kops-state-store".format(cluster_name)
    if not s3_exists_bucket(s3, bucket_name):
        log.info("Creating s3 bucket %s in region %s", bucket_name, region)
        s3.create_bucket(
            Bucket=bucket_name, CreateBucketConfiguration={"LocationConstraint": region}
        )
        log.info("Waiting for bucket creation to finish")
        s3.Bucket(bucket_name).wait_until_exists()
        log.info("Enabling bucket versioning")
        bucket_versioning = s3.BucketVersioning(bucket_name)
        bucket_versioning.enable()
        log.info("Enabling bucket encryption")
        s3_client = boto3.client("s3", region_name=region)
        s3_client.put_bucket_encryption(
            Bucket=bucket_name,
            ServerSideEncryptionConfiguration={
                "Rules": [
                    {"ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"}}
                ]
            },
        )

        create_cluster(cfg, kubernetes_version, enable_metrics)

        log.info(
            "Waiting for cluster to finish initialization. "
            "This takes about 5 minutes."
        )
        for i in range(0, 30):
            try:
                export_kubecfg(cluster_name, True)
                configure_kubernetes_client()
                v1 = client.CoreV1Api()
                nodes = v1.list_node()
                if len(nodes.items) >= 2:
                    break
            except Exception:
                pass
            log.debug("Attempt {}: cluster not yet ready", i)
            time.sleep(10)

    export_kubecfg(cluster_name, True)
    configure_kubernetes_client()

    deployments = [
        "calico-kube-controllers",
        "coredns",
        "coredns-autoscaler",
    ]
    if enable_metrics:
        deployments.append("metrics-server")
    for dname in deployments:
        tolerate_deployment_on_master(dname, "kube-system")

    kops("validate", "cluster", "--wait=5m", _out=sys.stdout)

    if enable_autoscaler:
        deploy_autoscaler(cfg)

    log.info("Cluster is up and running.")


def create_cluster(cfg, kubernetes_version, enable_metrics):
    # disable image digest because it often fails due to dns
    # timeouts/issues in the container
    flags = os.environ.get("KOPS_FEATURE_FLAGS", "")
    if flags:
        os.environ["KOPS_FEATURE_FLAGS"] = "%s,-ImageDigest" % flags
    else:
        os.environ["KOPS_FEATURE_FLAGS"] = "-ImageDigest"

    master_zones = cfg.aws["master_zones"]
    master_size = cfg.aws["master_size"]
    node_zones = cfg.aws["node_zones"]
    node_size = cfg.aws["node_size"]
    priv_key = read_sshkey(cfg.aws["ssh_private_key"])
    ssh_pub_key_content = pkey_to_pub_key(priv_key)
    if not ssh_pub_key_content:
        raise RuntimeError("Failed to load ssh key")
    with tempfile.NamedTemporaryFile(mode="w") as ssh_pub_key_file:
        ssh_pub_key_file.write(ssh_pub_key_content)
        ssh_pub_key_file.flush()
        ssh_pub_key_path = ssh_pub_key_file.name

        cluster_name = cfg.aws["cluster_name"]
        full_cluster_name = "{}.k8s.local".format(cluster_name)
        log.info("Creating K8s master and nodes on AWS")
        kops_args = [
            "create",
            "cluster",
            "--stderrthreshold",
            "0",
            "--kubernetes-version",
            kubernetes_version,
            "--networking=calico",
            "--zones",
            node_zones,
            "--master-zones",
            master_zones,
            "--master-size",
            master_size,
            "--node-count=1",  # 0 is not possible
            "--node-size",
            node_size,
            "--authorization=RBAC",
            "--cloud=aws",
            "--ssh-public-key",
            ssh_pub_key_path,
            "--name",
            full_cluster_name,
            "--dry-run",
            "-oyaml",
            "--v=4",
        ]
        vpc_id = cfg.aws.get("vpc_id", None)
        subnets = cfg.aws.get("subnets", None)
        ami_id = cfg.aws.get("ami", None)
        public_ip = cfg.aws.get("public_ip", None)
        if vpc_id:
            kops_args.extend(["--vpc", vpc_id])
        if subnets:
            kops_args.extend(["--subnets", subnets])
        if ami_id:
            kops_args.extend(["--image", ami_id])
        if public_ip is not None:
            if public_ip:
                kops_args.append("--associate-public-ip=true")
            else:
                if not vpc_id:
                    raise RuntimeError(
                        'Private IPs need a VPC to be specified with \
                        the value "aws.vpc_id" in the "config.yaml" file.'
                    )
                if not subnets:
                    raise RuntimeError(
                        'Private IPs need a subnet to be specified with \
                        the value "aws.subnets" in the "config.yaml" file.'
                    )
                kops_args.append("--associate-public-ip=false")
                kops_args.append("--api-loadbalancer-type=internal")
                kops_args.append("--topology=private")
                kops_args.extend(["--utility-subnets", subnets])
        output = kops(*kops_args)

        yaml_docs = [
            yaml.load(x, Loader=Loader) for x in output.split("---") if x.strip() != ""
        ]
        if enable_metrics:
            found_metrics = False
            for item in yaml_docs:
                if item["kind"] == "Cluster":
                    # this is currently broken in kops 1.23.0
                    item["spec"]["metricsServer"] = {"enabled": True, "insecure": True}
                    found_metrics = True
                    break
            if not found_metrics:
                raise RuntimeError("Cluster item not found")

        fname = None

        for item in yaml_docs:
            if item["kind"] == "InstanceGroup":
                # for configuration options, see
                # https://github.com/kubernetes/kops/blob/master/docs/instance_groups.md

                # allow V1 authentication to support old tooling
                imd = item["spec"].get("instanceMetadata", {})
                imd["httpTokens"] = "optional"
                item["spec"]["instanceMetadata"] = imd

                if item["spec"]["role"] == "Node":
                    # the maximum amount of time that a node instance can be in service
                    # before it is terminated
                    max_instance_lifetime = cfg.aws.get(
                        "asg_max_instance_lifetime", None
                    )
                    if max_instance_lifetime:
                        item["spec"]["maxInstanceLifetime"] = max_instance_lifetime

        # allow swap on nodes
        for item in yaml_docs:
            if item["kind"] == "Cluster":
                if "kubelet" not in item["spec"]:
                    item["spec"]["kubelet"] = {}
                item["spec"]["kubelet"]["failSwapOn"] = False

        with tempfile.NamedTemporaryFile(mode="w", delete=False) as f:
            fname = f.name
            for item in yaml_docs:
                f.write("---\n")
                yaml.dump(item, stream=f, Dumper=Dumper)

        log.debug("Creating cluster using file %s", fname)
        # this does not create the cluster yet
        kops("create", "-f", fname)
        os.remove(fname)

        kops(
            "create",
            "secret",
            "--name",
            full_cluster_name,
            "sshpublickey",
            "admin",
            "-i",
            ssh_pub_key_path,
        )

        # this will create resources in aws
        kops("update", "cluster", "--name", full_cluster_name, "--yes", _out=sys.stdout)
