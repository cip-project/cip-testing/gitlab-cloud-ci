# Copyright (c) Siemens AG, 2019 - 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os
import logging
import socket
import sh

from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.ssh import find_identify_file, read_sshconfig

log = logging.getLogger(__name__)


def run(args):
    """Creates new K3s deployment on the given host. The destination
    configuration extracted from the given ssh configuration.
    The destination argument must be equal to a 'Host' entry in the ssh
    configuration."""

    kubernetes_version = args.kubernetes_version

    cfg = load_config()
    ssh_config = read_sshconfig(cfg.ssh["host"], cfg.ssh["config_file"])

    hostname = ssh_config["hostname"]
    ips = list(
        i[4][0]  # raw socket structure  # internet protocol info  # address
        for i in socket.getaddrinfo(hostname, 0)
        if i[0] is socket.AddressFamily.AF_INET  # ipv4
        # ignore duplicate addresses with other socket types
        and i[1] is socket.SocketKind.SOCK_RAW
    )
    ip = ips[0]
    log.info("Host %s has ip %s", hostname, ip)

    k3sup_args = [
        "install",
        "--local-path",
        os.getenv("KUBECONFIG", "kubeconfig"),
        "--k3s-version",
        kubernetes_version,
        "--ip",
        ip,
        "--ssh-port",
        ssh_config["port"],
        "--user",
        ssh_config["user"],
        "--ssh-key",
        find_identify_file(ssh_config),
        "--k3s-extra-args",
        "--disable traefik",
    ]

    if cfg.ssh["sudo"] is True:
        k3sup_args.append("--sudo")
    sh.k3sup(*k3sup_args)

    log.info("Success! Your K3s cluster is up and running.")
