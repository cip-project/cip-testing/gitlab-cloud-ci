# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
from . import aws, ssh


def add_parsers(parent_parser):
    subparsers = parent_parser.add_subparsers(required=True, dest="subcommand")
    parser = subparsers.add_parser("aws", help="create k8s cluster on aws")
    # XXX: change this when upgrading
    # see https://kubernetes.io/releases/
    parser.add_argument(
        "--kubernetes-version",
        default="v1.30.9",
        help="specify K8s version",
        dest="kubernetes_version",
    )
    parser.add_argument(
        "--enable-metrics",
        default=False,
        action="store_true",
        help="deploy metrics server",
    )
    parser.add_argument(
        "--enable-autoscaler",
        default=True,
        action="store_true",
        help="deploy autoscaler addon",
    )
    parser.set_defaults(func=aws.run)

    parser = subparsers.add_parser("ssh", help="create k3s cluster using ssh")
    # XXX: change this when upgrading
    # see https://github.com/k3s-io/k3s/releases
    parser.add_argument(
        "--kubernetes-version",
        default="v1.30.9+k3s1",
        help="specify K3s version",
        dest="kubernetes_version",
    )
    parser.set_defaults(func=ssh.run)
