# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import sh
from siemens.gitlabci.config import load_config

from siemens.gitlabci.lib.kops import export_kubecfg


def run(args):
    """Remove Monitoring from the cluster"""

    namespace = args.namespace
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    sh.helm("uninstall", "grafana", "--namespace", namespace)
    sh.helm("uninstall", "prometheus", "--namespace", namespace)
