# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import base64


from kubernetes import client
from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client


def run(args):
    """get the generated admin password for grafana"""

    namespace = args.namespace
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    configure_kubernetes_client()

    core = client.CoreV1Api()
    secret = core.read_namespaced_secret("grafana", namespace)
    print(base64.b64decode(secret.data["admin-password"]).decode())
