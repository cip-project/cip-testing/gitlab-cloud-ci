# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
from . import deploy, get_admin_password, purge


def add_parsers(parent_parser):
    subparsers = parent_parser.add_subparsers(required=True, dest="subcommand")
    parser = subparsers.add_parser("deploy", help="deploy monitoring")
    parser.add_argument(
        "--namespace",
        default="monitoring",
        help="deploy in the given namespace",
        dest="namespace",
    )
    parser.set_defaults(func=deploy.run)

    parser = subparsers.add_parser(
        "get-admin-password", help="get the generated admin password for grafana"
    )
    parser.add_argument(
        "--namespace",
        default="monitoring",
        help="monitoring namespace",
        dest="namespace",
    )
    parser.set_defaults(func=get_admin_password.run)

    parser = subparsers.add_parser("purge", help="remove Monitoring from the cluster")
    parser.add_argument(
        "--namespace",
        default="monitoring",
        help="monitoring namespace",
        dest="namespace",
    )
    parser.set_defaults(func=purge.run)
