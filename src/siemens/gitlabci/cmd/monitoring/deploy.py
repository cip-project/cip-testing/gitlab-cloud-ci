# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import tempfile
import logging
import sh

from string import Template
from kubernetes import client, utils
from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.helm import install_helm_repo
from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.ssh import SSHSession
from siemens.gitlabci.lib.aws import on_aws
from siemens.gitlabci.lib.kubernetes import (
    configure_kubernetes_client,
    create_namespace,
)

log = logging.getLogger(__name__)


def run(args):
    """deploy monitoring pods"""

    namespace = args.namespace
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)

    sh.helm("repo", "add", "grafana", "https://grafana.github.io/helm-charts")
    sh.helm(
        "repo",
        "add",
        "prometheus",
        "https://prometheus-community.github.io/helm-charts",
    )
    sh.helm("repo", "update")

    configure_kubernetes_client()
    create_namespace(namespace)

    k8s_client_core = client.CoreV1Api()
    if not on_aws(k8s_client_core):
        cmd = "{sudo} mkdir -p /mnt/pv1 /mnt/pv2".format(
            sudo="sudo" if cfg.ssh["sudo"] is True else ""
        )
        ssh_session = SSHSession(
            cfg.ssh["host"],
            cfg.ssh.get("config_file", None),
        )
        try:
            ssh_session.execute_command(cmd, None)
        finally:
            ssh_session.close()

        k8s_client = client.ApiClient()
        utils.create_from_yaml(k8s_client, "share/k8s/prometheus/pv-volume.yaml")

    sh.helm(
        "install",
        "--name-template",
        "prometheus",
        "prometheus/prometheus",
        "--namespace",
        namespace,
        "-f",
        "./share/k8s/monitoring/prometheus.yaml",
    )
    with open("./share/k8s/monitoring/grafana.yaml", mode="r") as grafana_file:
        grafana_config = Template(grafana_file.read())

    overwrites = {"namespace": namespace}

    with tempfile.NamedTemporaryFile(mode="w") as temp_grafana_config:
        merged_grafana_config = grafana_config.substitute(overwrites)
        temp_grafana_config.write(merged_grafana_config)
        temp_grafana_config.flush()

        install_helm_repo("grafana", "https://grafana.github.io/helm-charts")
        install_helm_repo(
            "prometheus", "https://prometheus-community.github.io/helm-charts"
        )
        sh.helm(
            "install",
            "--name-template",
            "grafana",
            "grafana/grafana",
            "--namespace",
            namespace,
            "-f",
            temp_grafana_config.name,
        )
