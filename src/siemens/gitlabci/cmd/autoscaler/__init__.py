# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

from . import deploy
from . import purge
from . import status


def add_parsers(parent_parser):
    subparsers = parent_parser.add_subparsers(required=True, dest="subcommand")
    parser = subparsers.add_parser("deploy", help="display deploy")
    parser.set_defaults(func=deploy.run)
    parser = subparsers.add_parser("status", help="display status")
    parser.set_defaults(func=status.run)
    parser = subparsers.add_parser("purge", help="purge autoscaler")
    parser.set_defaults(func=purge.run)
