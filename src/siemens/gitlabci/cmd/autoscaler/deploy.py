# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import sh
import pathlib
import boto3

from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.aws import aws_iam_lookup_policy_arn, check_aws_response
from siemens.gitlabci.config import load_config

log = logging.getLogger(__name__)


def run(_args):
    """Deploy AWS autoscaler to dynamically adjust cluster size"""
    cfg = load_config()
    return _deploy(cfg)


def _deploy(cfg):
    # XXX: change this when upgrading
    # needs to be changed when Kubernetes is upgraded
    # https://github.com/kubernetes/autoscaler/blob/master/charts/cluster-autoscaler/Chart.yaml
    # NOTE: appVersion should match the K8s major.minor
    chart_version = "9.37.0"

    region = cfg.aws["region"]
    cluster_name = cfg.aws["cluster_name"]

    # auto-scaling group settings
    asg_policy_name = cfg.aws["asg_policy_name"]
    min_size = int(cfg.aws["asg_min_size"])
    max_size = int(cfg.aws["asg_max_size"])
    # how long a node should be unneeded before it is eligible for scale down
    scale_down_unneeded_time = cfg.aws["asg_scaledown_duration"]

    # kops creates two roles, nodes.$CLUSTER_NAME and masters.$CLUSTER_NAME
    # since the auto-scaler process runs on the master, we choose the latter one
    iam = boto3.client("iam", region_name=region)

    policy_arn = aws_iam_lookup_policy_arn(iam, asg_policy_name)
    if not policy_arn:
        log.info("Policy %s does not yet exist, creating now", asg_policy_name)
        response = iam.create_policy(
            PolicyName=asg_policy_name,
            PolicyDocument=pathlib.Path(
                "./share/k8s/autoscaler/asg-policy.json"
            ).read_text(),
        )
        check_aws_response(response)
        policy_arn = response["Policy"]["Arn"]

    full_cluster_name = "{}.k8s.local".format(cluster_name)
    iam_role = "masters.{}".format(full_cluster_name)
    log.info("Attaching IAM policy %s to role %s", policy_arn, iam_role)
    check_aws_response(iam.attach_role_policy(RoleName=iam_role, PolicyArn=policy_arn))

    client = boto3.client("autoscaling", region_name=region)

    # figure out the name of the nodes auto-scaling group
    response = client.describe_auto_scaling_groups()
    for ag in response["AutoScalingGroups"]:
        asg_name = ag["AutoScalingGroupName"]
        if asg_name.startswith("nodes") and asg_name.endswith(full_cluster_name):
            log.info("Updating configuration of autoscaling group %s", asg_name)
            check_aws_response(
                client.update_auto_scaling_group(
                    AutoScalingGroupName=asg_name,
                    MinSize=min_size,
                    MaxSize=max_size,
                    DesiredCapacity=max(0, min_size),
                    DefaultCooldown=10,
                )
            )
            check_aws_response(
                client.create_or_update_tags(
                    Tags=[
                        {
                            "ResourceId": asg_name,
                            "ResourceType": "auto-scaling-group",
                            "Key": "k8s.io/cluster-autoscaler/enabled",
                            "Value": "true",
                            "PropagateAtLaunch": True,
                        },
                        {
                            "ResourceId": asg_name,
                            "ResourceType": "auto-scaling-group",
                            "Key": "k8s.io/cluster-autoscaler/{}".format(cluster_name),
                            "Value": "true",
                            "PropagateAtLaunch": True,
                        },
                        {
                            "ResourceId": asg_name,
                            "ResourceType": "auto-scaling-group",
                            "Key": "Role",
                            "Value": "gitlab-cloud-ci",
                            "PropagateAtLaunch": True,
                        },
                    ]
                )
            )

    log.info("Deploying cluster-autoscaler")
    export_kubecfg(cluster_name)
    sh.helm("repo", "add", "autoscaler", "https://kubernetes.github.io/autoscaler")
    sh.helm("repo", "update")
    # see https://github.com/kubernetes/autoscaler/tree/master/charts/cluster-autoscaler
    sh.helm(
        "install",
        "autoscaler/cluster-autoscaler",
        "--version",
        chart_version,
        "--name-template",
        "aws-auto-scaler",
        "--namespace",
        "kube-system",
        "--set",
        "autoDiscovery.clusterName={},".format(cluster_name),
        "--set",
        "awsRegion={}".format(region),
        "--set",
        "extraArgs.scale-down-unneeded-time={}".format(scale_down_unneeded_time),
        "-f",
        "share/k8s/autoscaler/values.yaml",
    )
