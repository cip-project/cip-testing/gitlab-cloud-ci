# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import sh

from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.aws import delete_autoscaler_iam

log = logging.getLogger(__name__)


def run(_args):
    """Purge AWS autoscaler pod"""
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    region = cfg.aws["region"]
    asg_policy_name = cfg.aws["asg_policy_name"]

    export_kubecfg(cluster_name)

    log.info("Purging deployment cluster-autoscaler")
    sh.helm("uninstall", "aws-auto-scaler", "--namespace=kube-system")
    delete_autoscaler_iam(cluster_name, region, asg_policy_name)
