# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import sh

from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kops import export_kubecfg

log = logging.getLogger(__name__)


def run(_args):
    """Get status of cluster-autoscaler"""
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    print(sh.helm("status", "aws-auto-scaler", "--namespace=kube-system"))
