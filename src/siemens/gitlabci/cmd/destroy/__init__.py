# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
from . import aws, ssh


def add_parsers(parent_parser):
    subparsers = parent_parser.add_subparsers(required=True, dest="subcommand")
    parser = subparsers.add_parser("aws", help="destroy cluster on aws")
    parser.set_defaults(func=aws.run)

    parser = subparsers.add_parser("ssh", help="destroy cluster using ssh")
    parser.set_defaults(func=ssh.run)
