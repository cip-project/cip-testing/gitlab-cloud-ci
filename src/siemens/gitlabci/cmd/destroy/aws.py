# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import sys
import boto3
from siemens.gitlabci.config import load_config

from siemens.gitlabci.lib.kops import kops
from siemens.gitlabci.lib.aws import (
    aws_iam_lookup_policy_arn,
    delete_autoscaler_iam,
    s3_exists_bucket,
)

log = logging.getLogger(__name__)


def run(_args):
    """Destroy cluster running on AWS"""

    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    region = cfg.aws["region"]

    full_cluster_name = "{}.k8s.local".format(cluster_name)
    log.info("Tearing down cluster %s on AWS...", full_cluster_name)

    asg_policy_name = cfg.aws["asg_policy_name"]
    delete_autoscaler_iam(cluster_name, region, asg_policy_name)

    kops(
        "delete",
        "cluster",
        "--yes",
        "--name",
        full_cluster_name,
        _out=sys.stdout,
    )
    log.info("Cluster %s has been destroyed successfully", full_cluster_name)

    s3 = boto3.resource("s3", region_name=region)
    bucket_name = "{}-kops-state-store".format(cluster_name)
    if s3_exists_bucket(s3, bucket_name):
        log.info("Deleting s3 bucket %s in region %s", bucket_name, region)

        client = boto3.client("s3")
        paginator = client.get_paginator("list_object_versions")
        response_iterator = paginator.paginate(Bucket=bucket_name)
        for response in response_iterator:
            versions = response.get("Versions", [])
            versions.extend(response.get("DeleteMarkers", []))
            for key, version_id in [
                (x["Key"], x["VersionId"]) for x in versions if x["VersionId"] != "null"
            ]:
                log.info("Deleting %s version %s", key, version_id)
                client.delete_object(Bucket=bucket_name, Key=key, VersionId=version_id)

        s3.Bucket(bucket_name).delete()

    iam = boto3.client("iam", region_name=region)
    asg_policy_name = cfg.aws["asg_policy_name"]
    policy_arn = aws_iam_lookup_policy_arn(iam, asg_policy_name)
    if policy_arn:
        log.info("Deleting IAM policy %s", policy_arn)
        iam.delete_policy(PolicyArn=policy_arn)

    log.info("All done")
