# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging

from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.ssh import SSHSession

log = logging.getLogger(__name__)


def run(_args):
    """destroy cluster running on the given host"""

    cfg = load_config()
    destroy_cmd = "{sudo} k3s-uninstall.sh".format(
        sudo="sudo" if cfg.ssh["sudo"] is True else ""
    )
    ssh_session = SSHSession(
        cfg.ssh["host"],
        cfg.ssh.get("config_file", None),
    )
    try:
        # direct access for input
        ssh_session.execute_command(destroy_cmd, None)
    finally:
        ssh_session.close()
