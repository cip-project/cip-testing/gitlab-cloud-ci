# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import humanize

from datetime import datetime
from tzlocal import get_localzone
from kubernetes import client
from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.kubernetes import configure_kubernetes_client


def run(args):
    """List currently running CI jobs"""
    namespace = args.namespace
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    configure_kubernetes_client()
    _list(namespace)


def _list(namespace):
    v1 = client.CoreV1Api()
    job_fmt_str = "{name: <60}{age: <20}{title: <40}{who: <30}{url}"
    print(
        job_fmt_str.format(name="NAME", age="AGE", title="TITLE", who="WHO", url="URL")
    )
    for item in v1.list_namespaced_pod(namespace).items:
        if "app" not in item.metadata.labels:
            ci_job_url = ""
            ci_title = ""
            gitlab_user = ""
            for container in item.spec.containers:
                if container.env:
                    for entry in container.env:
                        if entry.name == "CI_JOB_URL":
                            ci_job_url = entry.value
                        elif entry.name == "CI_COMMIT_TITLE":
                            ci_title = entry.value
                        elif entry.name == "GITLAB_USER_NAME":
                            gitlab_user = entry.value
            start_time = item.status.start_time
            age = ""
            if start_time:
                age = datetime.now(get_localzone()) - start_time
            print(
                job_fmt_str.format(
                    name=item.metadata.name,
                    age=humanize.naturaldelta(age),
                    title=ci_title,
                    who=gitlab_user,
                    url=ci_job_url,
                )
            )
