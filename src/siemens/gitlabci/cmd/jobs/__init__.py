# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
from . import list


def add_parsers(parent_parser):
    subparsers = parent_parser.add_subparsers(required=True, dest="subcommand")
    parser = subparsers.add_parser("list", help="list currently running CI jobs")
    parser.add_argument(
        "--namespace",
        default="gitlab",
        help="kubernetes namespace to use",
        dest="namespace",
        type=str,
    )
    parser.set_defaults(func=list.run)
