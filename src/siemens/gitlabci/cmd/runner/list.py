# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import sh

from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.kops import export_kubecfg


def run(args):
    """List all runners in the given namespace"""
    namespace = args.namespace
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    _list(namespace)


def _list(namespace):
    """List all runners in the given namespace"""

    print(sh.helm("list", "--namespace", namespace))
