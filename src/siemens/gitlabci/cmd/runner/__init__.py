# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
import shtab
from . import deploy, list, purge


def add_parsers(parent_parser):
    subparsers = parent_parser.add_subparsers(required=True, dest="subcommand")
    parser = subparsers.add_parser("deploy", help="deploy a Gitlab runner")
    parser.add_argument(
        "--namespace",
        default="gitlab",
        help="deploy in the given namespace",
        dest="namespace",
    )
    parser.add_argument(
        "--token",
        help="Gitlab runner token",
        dest="token",
    )
    parser.add_argument(
        "--config",
        default="share/k8s/gitlab-runner/values.yaml",
        help="configuration file for the Gitlab runner",
        dest="config",
    ).complete = shtab.FILE
    parser.add_argument("name")
    parser.set_defaults(func=deploy.run)

    parser = subparsers.add_parser("list", help="list runners")
    parser.add_argument(
        "--namespace",
        default="gitlab",
        help="use the given namespace",
        dest="namespace",
    )
    parser.set_defaults(func=list.run)

    parser = subparsers.add_parser("purge", help="purge runners")
    parser.add_argument(
        "--namespace",
        default="gitlab",
        help="use the given namespace",
        dest="namespace",
    )
    parser.add_argument("names", nargs="+")
    parser.set_defaults(func=purge.run)
