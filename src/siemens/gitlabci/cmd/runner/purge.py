# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import json
import sh
from siemens.gitlabci.config import load_config

from siemens.gitlabci.lib.kops import export_kubecfg


def run(args):
    """Remove a Gitlab runner instance from the cluster"""
    names = args.names
    namespace = args.namespace
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)
    output = sh.helm("list", "--namespace", namespace, "--output=json")
    existing = set([x["name"] for x in json.loads(output)])
    for n in set(names).intersection(existing):
        sh.helm("uninstall", n, "--namespace", namespace)
