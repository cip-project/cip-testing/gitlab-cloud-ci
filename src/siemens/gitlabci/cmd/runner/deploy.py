# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import sh
import tempfile
import logging
import yaml

from deepmerge import always_merger

from siemens.gitlabci.config import load_config
from siemens.gitlabci.lib.helm import install_helm_repo
from siemens.gitlabci.lib.kops import export_kubecfg
from siemens.gitlabci.lib.kubernetes import (
    configure_kubernetes_client,
    create_namespace,
)

log = logging.getLogger(__name__)


def run(args):
    """Deploy a Gitlab runner instance into the cluster"""

    # XXX: change this when upgrading
    # see https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/v0.69.0/Chart.yaml
    chart_version = "0.73.3"

    name = args.name
    token = args.token
    namespace = args.namespace
    config = args.config
    cfg = load_config()
    cluster_name = cfg.aws["cluster_name"]
    export_kubecfg(cluster_name)

    overrides = {}
    if token:
        overrides["runnerToken"] = token

    log.info("Reading default values from file %s", config)
    with open(config, mode="r") as cfg_file:
        default_cfg = yaml.safe_load(cfg_file)

    merged_config = always_merger.merge(default_cfg, overrides)

    with tempfile.NamedTemporaryFile(mode="w") as temp:
        yaml.dump(merged_config, temp)
        temp.flush()

        configure_kubernetes_client()
        create_namespace(namespace)

        install_helm_repo("gitlab", "https://charts.gitlab.io")
        sh.helm(
            "install",
            "gitlab/gitlab-runner",
            "--version={}".format(chart_version),
            "--namespace",
            namespace,
            "--name-template",
            name,
            "--values",
            temp.name,
        )
