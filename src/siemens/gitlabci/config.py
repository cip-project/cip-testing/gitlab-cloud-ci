# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import yaml

log = logging.getLogger(__name__)


class Config:
    def __init__(self, fpath):
        with open(fpath) as f:
            self.__cfg = yaml.safe_load(f)

    def __getattr__(self, name):
        return self.__cfg[name]


def load_config():
    return Config("config.yaml")
