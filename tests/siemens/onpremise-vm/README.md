# On-Premise Test VM

This directory contains the necessary files to setup a test VM to test the on-premise commands.

## Setup

- [qemu](https://www.qemu.org/)
- [cloud-utils](https://github.com/canonical/cloud-utils/)
- [just](https://github.com/casey/just)

## Preparation

This needs to be done only once:

```bash
just bootstrap
```

## Start VM

```bash
just start
```

This will create an overlay (*.ovl).
If you want to start fresh, run `just clean`.
