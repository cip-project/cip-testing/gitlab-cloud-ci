# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

from siemens.gitlabci.config import Config


def test_load_config():
    cfg = Config("./config.yaml.sample")

    aws_keys = [
        "ssh_private_key",
        "cluster_name",
        "region",
        "master_zones",
        "master_size",
        "node_zones",
        "node_size",
        "asg_policy_name",
        "asg_min_size",
        "asg_max_size",
        "asg_scaledown_duration",
        "asg_max_instance_lifetime",
    ]
    for key in aws_keys:
        assert key in cfg.aws
    assert len(cfg.aws) == len(aws_keys)
