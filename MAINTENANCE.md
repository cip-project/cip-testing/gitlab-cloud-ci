# Cluster Maintenance

Typically you will want to fine-tune certain cluster settings (e.g. node instance type) because the current settings do not fit your workload properly.

`gitlabci` only exposes a subset of the available cluster management commands.

However, `just shell` will start a Docker container in which many tools such as `helm` are available without any extra configuration,
e.g. commands like `kubectl get nodes` work out of the box.

* [Restore kubectl credentials](#restore-kubectl-credentials)
* [Modify Gitlab Runners](#modify-gitlab-runners)
* [Modify Auto-Scaling Group Configuration](#modify-auto-scaling-group-configuration)
* [Modify EC2 instance type for nodes](#modify-ec2-instance-type-for-nodes)

## Restore kubectl credentials

**AWS**: In case you have lost your local kube config file, you can re-create it via `kops export kubecfg --state "s3://${CLUSTER_NAME}-kops-state-store"  --name "${CLUSTER_NAME}.k8s.local" --admin`.

By default, the credentials of any exported admin user have a **lifetime of 18 hours**.
The lifetime of the exported credentials may be specified as a value of the `--admin` flag, e.g. `--admin=87600h`.

## Modify Gitlab Runners

Just add a new runner and then remove the old one.

## Modify Auto-Scaling Group Configuration

Purge and then re-deploy the auto-scaler add-on.

## Modify EC2 instance type for nodes

```bash
kops get instancegroups
kops edit ig nodes-eu-central-1a
# review changes:
kops update cluster
# then apply:
kops update cluster --yes
```

**Notice:** You have to re-deploy the auto-scaler add-on afterwards because `kops` overrides certain tags in the ASG configuration.

## kubectl Cheatsheet

```bash
kubectl get nodes -o json

kubectl get pods -A

# follow auto-scaler logs
kubectl -n kube-system logs -f -l app.kubernetes.io/name=aws-cluster-autoscaler

alias k="kubectl --namespace gitlab"
k get pods

k describe pod 1687162855-cip-project-v12-project-gitlab-runner-b57965b55q2gjv
```

# Kubernetes Upgrade

Grep for `XXX:` and update the values accordingly.
