# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
{ pkgs, gitlabci, runtimeDeps }:

pkgs.dockerTools.buildLayeredImage {
  name = "registry.gitlab.com/cip-project/cip-testing/gitlab-cloud-ci";
  tag = "latest";

  config = with pkgs; {
    Entrypoint = let myZshrc = writeText "zshrc" (builtins.readFile ./share/zshrc); in
      [
        (writeShellScript "entrypoint" ''
          ${dockerTools.shadowSetup}
          mkdir -p /usr /root /tmp
          ln -s /bin /usr/bin
          ln -s ${myZshrc} /root/.zshrc
          ln -s /etc/ssl/certs/ca-bundle.crt /etc/ssl/certs/ca-certificates.crt
          exec ${zsh}/bin/zsh --login --interactive
        '')
      ];
    WorkingDir = "/src";
    Env = [
      "HELM_TLS_ENABLE=true"
      "HOME=/root"
      "TMPDIR=/tmp"
    ];
  };

  contents = with pkgs; [
    busybox # ls, env, etc.
    cacert # needed to access s3
    gitlabci
    runtimeDeps # convenient to have them in PATH
    (python3.withPackages
      (ps: with ps; [
        boto3
        coloredlogs
        deepmerge
        humanize
        kubernetes
        paramiko
        pyyaml
        sh
        shtab
        tzlocal
      ]))
  ];

}
