# Copyright (c) Siemens AG, 2023
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
from setuptools import setup

# see https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html
setup()
