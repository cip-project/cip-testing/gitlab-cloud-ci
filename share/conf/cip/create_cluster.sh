#!/bin/sh
###############################################################################
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
###############################################################################
set -eux

gitlabci create aws
gitlabci dashboard deploy

SCRIPT_DIR=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)

"$SCRIPT_DIR"/deploy_runners.sh
