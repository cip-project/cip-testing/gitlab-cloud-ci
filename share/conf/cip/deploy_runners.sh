#!/bin/sh
###############################################################################
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
###############################################################################
set -eu

SCRIPT_DIR=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)

EPOCH=$(date "+%s")

case "$CLUSTER_NAME" in
    *small*)
        CLUSTER_TYPE=small
        TOKEN_CIP_PROJECT=$TOKEN_CIP_PROJECT_SMALL
        TOKEN_CIP_PLAYGROUND=$TOKEN_CIP_PLAYGROUND_SMALL
        TOKEN_CIP_GITLAB_CLOUD_CI=$TOKEN_CIP_GITLAB_CLOUD_CI_SMALL
        ;;
    *)
        CLUSTER_TYPE=large
        TOKEN_CIP_PROJECT=$TOKEN_CIP_PROJECT_LARGE
        TOKEN_CIP_PLAYGROUND=$TOKEN_CIP_PLAYGROUND_LARGE
        TOKEN_CIP_GITLAB_CLOUD_CI=$TOKEN_CIP_GITLAB_CLOUD_CI_LARGE
        ;;
esac

# dots are not allowed in the runner name
RUNNER_NAME="$EPOCH-$CLUSTER_NAME-project"
RUNNER_NAME=$(echo "$RUNNER_NAME" | sed -e 's/\./-/g')
./gitlabci runner deploy "--token=$TOKEN_CIP_PROJECT" "--config=$SCRIPT_DIR/$CLUSTER_TYPE/runner.yaml" "$RUNNER_NAME"

RUNNER_NAME="$EPOCH-$CLUSTER_NAME-playground"
RUNNER_NAME=$(echo "$RUNNER_NAME" | sed -e 's/\./-/g')
./gitlabci runner deploy "--token=$TOKEN_CIP_PLAYGROUND" "--config=$SCRIPT_DIR/$CLUSTER_TYPE/runner.yaml" "$RUNNER_NAME"

RUNNER_NAME="$EPOCH-$CLUSTER_NAME-gitlab-cloud-ci"
RUNNER_NAME=$(echo "$RUNNER_NAME" | sed -e 's/\./-/g')
./gitlabci runner deploy "--token=$TOKEN_CIP_GITLAB_CLOUD_CI" "--config=$SCRIPT_DIR/$CLUSTER_TYPE/runner.yaml" "$RUNNER_NAME"
