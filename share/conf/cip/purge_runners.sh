#!/bin/sh
###############################################################################
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
###############################################################################
set -eu

FORCE=false

usage() {
    echo "Usage: $0 [-f]" 1>&2
    exit 1
}

while getopts ":f" o; do
    case "$o" in
        f)
            FORCE=true
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND - 1))

ask_confirm() {
    if [ ! "$FORCE" = "true" ]; then
        while true; do
            printf "Command: %s
Continue (y/n)? " "$*"
            read -r choice
            case "$choice" in
                y | Y) break ;;
                n | N) return 0 ;;
                *) echo "invalid" ;;
            esac
        done
    fi
    "${@}"
}

ALL_RUNNERS=$(gitlabci runner list | tail -n +2 | sort -V | awk '{print $1;}')
for runner_name in $ALL_RUNNERS; do
    ask_confirm gitlabci runner purge "$runner_name"
done
