# Copyright (c) Siemens AG, 2022
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0

set export := true
set dotenv-load := true

THISDIR := justfile_directory()
DOCKER := env_var_or_default("DOCKER", "docker")
DOCKER_IMAGE := "registry.gitlab.com/cip-project/cip-testing/gitlab-cloud-ci"

@default:
    just --list

# Launch container shell
shell CONFIG="config.yaml" PULL="always":
    #!/usr/bin/env bash
    set -euo pipefail
    VERSION=${VERSION:-v$(cat VERSION.txt)}
    EXTRA_ARGS=""
    LOCAL_ENV={{ join(THISDIR, ".local.env") }}
    [ ! -f $LOCAL_ENV ] || EXTRA_ARGS="$EXTRA_ARGS --env-file=$LOCAL_ENV"
    [ -z "${AWS_DEFAULT_REGION:-}" ] || EXTRA_ARGS="$EXTRA_ARGS --env AWS_DEFAULT_REGION"
    [ -z "${AWS_ACCESS_KEY_ID:-}" ] || EXTRA_ARGS="$EXTRA_ARGS --env AWS_ACCESS_KEY_ID"
    [ -z "${AWS_SECRET_ACCESS_KEY:-}" ] || EXTRA_ARGS="$EXTRA_ARGS --env AWS_SECRET_ACCESS_KEY"
    [ -z "${AWS_SESSION_TOKEN:-}" ] || EXTRA_ARGS="$EXTRA_ARGS --env AWS_SESSION_TOKEN"
    [ -z "${https_proxy:-}" ] || EXTRA_ARGS="$EXTRA_ARGS --env https_proxy"
    [ -z "${http_proxy:-}" ] || EXTRA_ARGS="$EXTRA_ARGS --env http_proxy"
    [ -z "${no_proxy:-}" ] || EXTRA_ARGS="$EXTRA_ARGS --env no_proxy"
    mkdir -p secrets/kube && chmod 700 secrets
    command -v yq || {
        echo "Please install yq from https://github.com/mikefarah/yq"
        exit 1
    }
    export CLUSTER_NAME=$(yq -r .aws.cluster_name < {{ CONFIG }})
    ZSH_HISTORY={{ join(THISDIR, "secrets", "zsh_history") }}
    touch "$ZSH_HISTORY"
    exec {{ DOCKER }} run --rm \
        --pull={{ PULL }} \
        --env CLUSTER_NAME \
        --env "KUBECONFIG=/src/secrets/kube/${CLUSTER_NAME}" \
        --volume {{ THISDIR }}:/src \
        --volume {{ join(THISDIR, CONFIG) }}:/src/config.yaml:ro \
        --volume "$ZSH_HISTORY:/root/.zsh_history" \
        --volume /etc/localtime:/etc/localtime:ro \
        $EXTRA_ARGS \
        -it {{ DOCKER_IMAGE }}:${VERSION}

# Format code
@format:
    black src tests

# Lint code
@lint:
    flake8 --max-line-length=88 src tests

# Run tests
@test:
    python3 -m pytest tests

# Build container image
build-container:
     {{ DOCKER }} load -i $(nix build --print-out-paths --no-link '.#docker')

# Generate shell completion
@generate:
    ./gitlabci completion zsh > share/completion/gitlabci.zsh
    ./gitlabci completion bash > share/completion/gitlabci.bash

# Cleanup cache files
@clean:
    rm -rf .pytest_cache
    find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

# Open K8s dashboard in your browser
@dashboard:
    xdg-open http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:https/proxy/#/login
